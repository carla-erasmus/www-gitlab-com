---
layout: markdown_page
title: "Analyst Relations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Analyst relations at GitLab

Analyst relations (AR) is generally considered to be a corporate strategy, communications and marketing activity, but at GitLab, because of our mission that everyone can contribute, we view the analyst community as participants as well. The primary owner of Analyst Relations at GitLab is Product Marketing, as they are the conduit between the analysts and the internal company information.  

Industry analysts usually focus on corporate or organizational buyers and their needs rather than on those of individual developers. At GitLab we view them as a marketing focus for those markets. They amplify our message to a different group than our traditional developer communities, although there is overlap to some degree between those communities.

### How we interact with the analyst community

Examples of how we engage with analysts include:
- Schedule briefings where we update analysts on our product, our company, our future direction and strategy;
- Answer questions for analyst reports such as Gartner MQs or Forrester Waves that provide in-depth information on product features, customer references and company information;
- Use analyst reports such as Gartner MQs or Forrester Waves that feature GitLab to help enhance or clarify our story for customers and partners;
- Provide our analyst newsletter to update analysts on what GitLab is doing between briefings;
- Schedule inquiries where analysts answer specific questions we have about products, markets, or strategies we want to understand better;
- Schedule consulting days for an extended dive with an analyst into products, markets, or strategies we are working on;
- Invite analysts to participate in webinars, speaking engagements, quotes for media, or other events where an analyst presence would be beneficial; and
- Hire analyst’s market research departments to help us create, run, and interpret survey research that helps us target markets or develop products optimally.

### Accessing analyst reports

Most analyst companies charge for access to their reports. 

-   If GitLab purchases reprint rights to a report, then that link will be available here, on the [Analyst Relations web page](https://about.gitlab.com/analysts/), and on the relevant product page. Reprint rights are the rights to share the link to the report - these generally last six months to one year.
-   GitLab maintains relationships with some analyst companies that provide us with access to some or all of their relevant research. These reports are for internal use only and sometimes limited to named individuals.  Those reports are generally kept in an internal [GitLab folder for analyst relations](https://drive.google.com/drive/u/0/folders/1oFmtmoXsbjMb6IuPIgIIZ-MG-tLfOjpw). If you are a GitLabber and you need access to a particular report, please reach out to [Analyst Relations](mailto:jtompsett@gitlab.com) and I'll help you find the research you need.


## What the analysts are saying about GitLab

This section contains highlights from areas where the analysts have rated GitLab in comparison to other vendors in a particular space. The highlights and lessons learned are listed here.  For more information click the link to go to the page dedicated to that report.

## [The Forrester New Wave™: Value Stream Management Tools, Q3 2018](https://about.gitlab.com/analysts/forrester-vsm/)

### Forrester's Take (taken from the report directly)

'[Forrester's]'' evaluation found that:

GitLab combines end-to-end capability with the power of open source. GitLab offers a DevOps tool for each step of the software development process. Top-level views sitting across these tools provide its VSM functionalities. 

GitLab needs greater depth with mapping and governance. GitLab's basic mapping and governance capabilities lag behind other VSM competitors'. With an end-to-end tool chain, GitLab should offer more comprehensive traceability and process governance. 

GitLab is best for companies that are looking for a broad, integrated solution.Organizations that want a comprehensive VSM solution that can also serve as their DevOps tool chain will really appreciate GitLab. GitLab's vision is firmly open source and dev-centric; companies that live and breathe dev-first will appreciate this approach. 

### Lessons Learned (GitLab's learning from this report)

**Mapping**

The VSM model encourages organizations to map business value, people, processes,
and data across value streams. In particular, this aids in efficient resource allocation,
and visibility of areas requiring urgent improvement.

GitLab is well-positioned to help organizations in this area since many of the required
building blocks are already built into GitLab. These include issues, boards, merge
requests, Git artifacts (commits and branches), and pipelines. GitLab needs to improve
integrating these pieces together for the purpose of mapping value streams, by building
a layer of seamless functionality, in order to provide that mapping visibility.


**Analytics and Visualization**

The value streams in VSM need to be based on consistent and reliable data. The VSM
solution needs to capture this data as the processes are excecuted, synthesize it
into actionable insight, and present the results in useful visualizations appropriate
for the various team members and stakeholders in an organization. For example, these
can be detailed metrics relevant to a small development team such as a burndown
chart, or a director-level or executive-level rollup such as an executive-level
dashbord showing mean total cycle delivery times to customer in the past 8 quarters.
Additionally, the VSM solution needs to be predictive based on historical data.
It should account for what-if scenarios and risk assessments, considering alternative
business decisions or process iterations.
We are focused on bringing this functionality to GitLab. In particular, we will
build the features in their relevant and optimal locations in GitLab. For example,
burndown charts will be integrated into issue boards, where development teams are
already working. Director-level dashboards, on the other hand will be highly customizable
and located at the group level, where the scope can span many children/descendant
groups and projects reflecting the organization structure as needed.


**Governance**

Big enterprises rely on a governance framework to mitigate a variety of risks. If
an organization breaks a controlled process while shipping a new piece of software,
which in turn results in a catastrophic event, causing a significant business interruption
or loss of business potential, that value delivered from the shipped software is
rendered nil and actually negative.

GitLab therefore needs to support these enterprise governance standards through
features such as enforced workflows, approvals, templating of various artifacts
like issues and code files, compliance, traceability of requirements, and
auditing/logging.



## Analyst reports that can help you deepen your knowledge
Analysts have opinions. They look at the technology world across industries and use cases and individual technologies. These reports here are listed for you as they may help you expand your understanding of a topic. These are behind the firewall and for use of employees where they have access rights. If you have any questions on access, please contact [Analyst Relations](mailto:jtompsett@gitlab.com).

#### DevOps, Agile, and Waterfall
This Gartner Group report speaks to how DevOps can enable Mode 2 in their Bimodal mode view of the world. Many enterprise clients share Gartner's view that the GitLab way of developing apps is a Mode 2 approach.
- [DevOps Is the Bimodal Bridge, 13 April 2017](https://drive.google.com/file/d/1yU6b3qlrLGbunhx48gDn5iQRfNB1QdcG/view?usp=sharing)

This Gartner Group report is a good report for getting an understanding and grounding in what DevOps means in the industry. Gartner agrees there are conflicting definitions and no standards body, so it supplies some terms and good ideas around the impact of DevOps usage in organizations.
- [Principles and Practices of DevOps that I&O Leaders Need to Cultivate, 12 January 2017](https://drive.google.com/file/d/1lQMUbRK3dgnwVhAT573cv3kvIpIN5tm_/view?usp=sharing)

This Gartner Group report helps define what the waterfall approach is and why an agile or iterative, or incremental approach is better. It helps clarify distinctions between various approaches.
- [The End of the Waterfall as We Know It, 12 June 2017](https://drive.google.com/file/d/1UcexVUy6OH0Y8yB8rWbZveVRbynHwciM/view?usp=sharing)

Some organizations get to Scrum and think this is agile computing. This Gartner Group piece makes the case for full DevOps or scaling agile and discusses what this means and its implications to the organization. 
- [Scrum Is Not Enough:XP Practices and an Agile Mindset Are Required, 2 August, 2018](https://drive.google.com/file/d/1v9G6eodV5s2k5pP7J9J3am4lIUCOeRdm/view?usp=sharing)


### Areas for expanded coverage

As GitLab grows and our product capabilities expand, we are engaging with more analysts in more companies, on a wider array of topics. The following is a list of topics and analyst companies appearing on our radar. 

#### Forrester:

##### Continuous Integration Tools
- Leaders: GitLab CI, CloudBees Jenkins, CircleCI, Microsoft
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Integration+Tools+Q3+2017/-/E-RES137261)

##### Configuration Management Softwaree for Infrastructure Automation
- Leaders: Puppet Enterprise, Chef Automate
- [Link to report](https://reprints.forrester.com/#/assets/2/675/'RES137964'/reports)

##### Modern Application Functional Test Automation Tools

- Leaders: Parasoft, IBM, Tricentis, HPE
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Modern+Application+Functional+Test+Automation+Tools+Q4+2016/-/E-RES123866)

##### Application Security Testing
- Leaders: Synopsys, CA Veracode
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Static+Application+Security+Testing+Q4+2017/-/E-RES139431)

##### Strategic Portfolio Management Tools
- Leaders: AgileCraft, CA, ServiceNow
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Strategic+Portfolio+Management+Tools+Q3+2017/-/E-RES136707)

##### Portfolio Management For The Tech Management Agenda
- Leaders: CA, Planview
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Portfolio+Management+For+The+Tech+Management+Agenda+Q1+2015/-/E-RES114742)

##### Enterprise Collaborative Work Management
- Leaders: Clarizen, Redbooth, Wrike, Planview, Asana, and Smartsheet
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Enterprise+Collaborative+Work+Management+Q4+2016/-/E-RES121721)

##### Application Life-Cycle Management, Q4 2012
- Covered in report: Atlassian, CollabNet, HP, IBM, Microsoft, PTC, Rally Software, Rocket Aldon, and Serena Software
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Application+LifeCycle+Management+Q4+2012/-/E-RES60080)

#### Gartner: 

##### Software Change and Configuration Management Software
- Market Guide: Amazon Web Services, Atlassian, BitKeeper, CA Technologies, Codice Software, Collabnet, GitHub, GitLab, IBM, Micro Focus/Borland, Microsoft, Perforce, PTC, SeaPine Software, Serena, SourceGear, Visible Systems, WANdisco, Wildbit
- [Link to report](https://www.gartner.com/document/3118917)
- Market Guide update initiated March 2018.

##### Continuous Configuration Automation Tools
- Market Guide: Chef, CFEngine, Inedo, Orca, Puppet, Red Hat, SaltStack
- [Link to report](https://www.gartner.com/document/3843365)

##### Software Test Automation
- Leaders: MicroFocus, Tricentis
- [Link to 2016-11-15 report](https://www.gartner.com/document/3512920)
- [Link to 2017-11-20 report](https://www.gartner.com/document/3830082)

##### Performance Testing
- Market Guide: Automation Anywhere, BlazeMeter, Borland, CA Technologies, HPE, IBM, Neotys, Oracle, Parasoft, RadView, SmartBear, Soasta, Telerik, TestPlant
- [Link to report](https://www.gartner.com/document/3133717)

##### Application Performance Monitoring Suites
- Leaders: New Relic
- [Link to report](https://www.gartner.com/doc/3551918)

##### Application Security Testing
- Leaders: Micro Focus, CA Technologies (Veracode), Checkmarx, Synopsys, IBM
- [Link to 2017-02-28 report](https://www.gartner.com/doc/3623017)
- [link to 2018-03-19 report](https://www.gartner.com/doc/3868966/magic-quadrant-application-security-testing)

##### Project Portfolio Management
- Leaders: Planview, CA Technologies, Changepoint
- [Link to report](https://www.gartner.com/document/3728917)

##### Enterprise Agile Planning Tools
- Leaders: CA, Atlassian, VersionOne
- [Link to report](https://www.gartner.com/doc/3695417)
- Research for next MQ commences Summer 2018

##### Container Management Software
- Market Guide: Apcera, Apprenda, CoreOS, Docker, Joyent, Mesosphere, Pivotal, Rancher Labs, Red Hat
- [Link to report](https://www.gartner.com/document/3782167)

##### Enterprise Application Platform as a Service
- Leaders: Salesforce, Microsoft
- [Link to report](https://www.gartner.com/document/3263917)

##### Data Science Platforms
- Leaders: IBM, SAS, RapidMiner, KNIME
- [Link to report](https://www.gartner.com/doc/3606026)