---
layout: markdown_page
title: "2018 Q4 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV according to plan. Sales prospects. Marketing achieves the SCLAU plan. Measure pipe-to-spend for all marketing activities.

* VPE
  * Support
      * Amer East: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * Amer West: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * APAC: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * EMEA: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
* CMO: Marketing achieves the SCLAU plan. 8 SCLAU per SDR per month, content 2x QoQ, website information architecture helps 50% QoQ.
* CMO: Measure pipe-to-spend for all marketing activities. Content team based on it, link meaningful conversations to pipe, double down on what works.

### CEO: Popular next generation product. Finish 2018 vision of entire DevOps lifecycle. GitLab.com is ready for mission critical applications. All installations have a graph of DevOps score vs. releases per year.

* VPE: Make GitLab.com ready for mission critical customer workloads
  * Frontend:
  * Dev Backend:
    * Gitaly:
      * Preserve 100% of [error budget]: 100% ()
      * Deliver first iteration of Object Deduplication work
    * Gitter:
      * Preserve 100% of [error budget]: 100% ()
      * Shut down billing and embeds on `apps-xx`
    * Plan:
      * Preserve 100% of [error budget]: 100% ()
      * Complete phase 1 of preparedness for [Elasticsearch on GitLab.com]
    * Create:
      * Preserve 100% of [error budget]: 100% ()
    * Manage:
      * Preserve 100% of [error budget]: 100% ()
    * Geo:
      * Preserve 100% of [error budget]: 100% ()
      * Complete all issues in Phase 1 of the DR/HA plan working with the Production Team ()
    * Distribution:
      * Preserve 100% of [error budget]: 100% ()
      * [Automate library upgrades](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/194) across distributed projects
      * Release management
        * Automated deployments on staging server
  * Ops Backend:
    * Configure: Preserve 100% of [error budget]: 100% ()
    * Configure: Merge smaller changes more frequently: &gt; 10 average MRs merged per week, &lt; 100 average lines added, &lt; average 7 files changed
    * Secure:
      * Preserve 100% of [error budget]: 100% ()
      * Merge smaller changes more frequently: &gt; 4 average MRs merged per week
    * [Philippe Lafoucrière](https://about.gitlab.com/team/#plafoucriere): Benchmark 3 new security tools for integration in the [Security Products](https://about.gitlab.com/handbook/engineering/ops-backend/secure/#security_products)
    * [Philippe Lafoucrière](https://about.gitlab.com/team/#plafoucriere): One CFP (Call For Proposals) accepted for a Tech conference
    * Monitor:
      * Preserve 100% of [error budget]: 100% ()
      * Merge smaller changes more frequently: &gt; 6 average MRs merged per week
  * Infrastructure: Make GitLab.com ready for mission critical workloads
    * Preserve 100% of [error budget]: 100% ()
    * Deliver streamlined and cleaned-up Infrastructure Handbook
    * SAE: Minimize GitLab.com's MTTR
      * Deliver observable availability improvements in database backup/restore/verification and replication
      * Launch `production` queue management for changes, incidents, deltas and hotspots
    * SRE: Maximize GitLab.com's MTBF
      * Deliver phase 1 DR and HA plan for GitLab.com
      * Implement roadmap and milestone planning with tracking of velocity
    * Andrew: Observable Availability
      * Deliver General Service Metrics and Alerting, Phase 2: error budgets dashboardI and attribution, alert actionability metrics, and MTTD metrics
      * Deliver [Distributed Tracing working in GDK](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4808)
      * Deliver DDoS ELK alerting, documentation and training
  * Quality:
  * Support
    * Amer East: Develop at least 2 Premium service process improvements focused on improved coordination with TAMS, Premium customer ticket reduction efforts, and create premium ticket reports that give us more clarity of needs within our Premium customer base.
    * Amer West: Create a *Support Engineer - GitLab.com* Bootcamp more tightly aligned with Production Engineering
    * APAC: TBD process improvement (e.g. SLA performance, Customer Experience, or Employee Development)
    * EMEA: Enable support team to create 30 updates to docs to help capture knowledge and improve customer self service. Track contributions and views to measure 'self service score' (support edited doc visitors / customers opening tickets).
  * UX:
    * Create a seamless cluster experience. Identify 3 additional ways users can create and manage their clusters.
    * [Complete a Competitive analysis of version control tools for designers (Abstract, Invision, Figma, etc.). Identify ways GitLab can leverage Open Source and our own tools to assist designers using GitLab.](https://gitlab.com/gitlab-org/gitlab-ce/issues/51654)
    * UX Research
      * [Establish a standardised process for Customer Focus Groups. Test and iterate on the process with at least 2 Enterprise customers.](https://gitlab.com/gitlab-org/ux-research/issues/98)
  * Security:
    * Security Operations:
      * Document at least 5 runbooks for logging sources: X/5, Y%
      * Security and Priority labels on 50 backlogged security issues: X/50, Y%
    * Application Security:
      * Complete at least 2 cycles of security release process: X/2, Y%
      * Conduct at least 3 application security reviews: X/3, Y%
      * Public HackerOne bounty program by December 15: X%
    * Compliance:
      * Document current procedures for each of the 14 ISO Operations Security Domain: X/14, Y%
      * Conduct gap analysis of at least one GitLab.com subnet's access controls: X/1, Y%
    * Abuse:
      * Port existing 'janitor' project to Rails w/ 100% feature parity: X/9, Y%
      * Support at least 2 external requests per month: X/6, Y%

### CEO: Great team. ELO score per interviewer, executive dashboards for all key results, train director group.

* CMO: Hire missing directors. Operations, Corporate, maybe Field.
* VPE: Craft an impactful iteration to improve our career development framework and train the team
* VPE: Source 50 candidates by Oct 15 and hire two Directors
  * Frontend:
  * Dev Backend:
    * Increase viable candidate pool for backend developer by X% (measurement TBD)
    * Increase offer rate for viable candidates by X% (measurement TBD)
    * Gitaly:
      * Source 10 candidates by October 15 and hire 1 developer: X sourced (X%),
        X hired (X%)
    * Gitter:
      * Create hiring process for fullstack developer
    * Plan:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: X sourced (X%), X hired (X%)
    * Create:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: X sourced (X%), X hired (X%)
      * Get 10 MRs merged into Go projects by team members without pre-existing
        Go experience: X merged (X%)
    * Manage:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: X sourced (X%), X hired (X%)
      * Team to deliver 10 topic-specific knowledge sharing sessions
        (“201s”) by the end of Q4 (X completed)
      * Geo:
        * At least one Geo team member to advance to Geo Maintainer ()
    * Distribution
      * Source 25 candidates (at least 5 by direct manager contact) by
      October 15 and hire 2 developers: X sourced (X%), X hired (X%)
    * Stan:
      * [Reduce baseline memory usage in Rails by 30%](https://gitlab.com/gitlab-org/gitlab-ce/issues/49702)
      * [Reduce runtime memory usage in Sidekiq for top 5 workers by 30%](https://gitlab.com/gitlab-org/gitlab-ce/issues/49703)
      * Resolve 3 P1 Tier 1 customer issues
  * Infrastructure: Source 10 candidates by Oct 15 and hire 1 SRE (APAC) manager: X sourced (X%) X hired (X%)
    * SAE: Source 10 candidates by Oct 15 and hire 1 DBRE: X sourced (X%) X hired (X%)
    * SRE: Source 25 candidates by Oct 15 and hire 2 SREs: X sourced (X%) X hired (X%)
  * Ops Backend:
    * Configure: Source 60 candidates by Nov 15 and hire 2 developers: 0 sourced (0%), hired 0 (0%)
    * Monitor: Source 30 candidates (at least 5 by direct manager contact) by November 15 and hire 3 developers: 0 sourced (0%), hired 0 (0%)
    * Secure: Source 75 candidates and hire 5 developers: X sourced (X%), X hired (X%)
  * Quality:
    * Source 100 candidates (at least 5 by direct manager contact) by
      October 15 and hire 2 test automation engineers: X sourced (X%), X hired (X%)
  * Security:
  * Support: Source 30 candidates by October 15 and hire 1 APAC Manager
    * Amer East: Source 30 candidates by October 15 and hire 1 Support Agent
    * Amer West: Source 30 candidates by October 15 and hire 1 Support Engineer
    * APAC: Source 30 candidates by October 15 and hire 1 Support Engineer
  * UX:

[error-budget]: /handbook/engineering/#error-budgets
[Elasticsearch on GitLab.com]: https://gitlab.com/groups/gitlab-org/-/epics/153
[Review Apps for CE and EE with GitLab QA running]: https://gitlab.com/groups/gitlab-org/-/epics/265
[Cross-browser and mobile browser test coverage]: https://gitlab.com/gitlab-org/quality/team-tasks/issues/45
[Addressing database discrepancy for our on-prem customers]: https://gitlab.com/gitlab-org/gitlab-ce/issues/51438
