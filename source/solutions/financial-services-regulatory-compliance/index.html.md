---
layout: markdown_page
title: "Financial Services Regulatory Compliance"
---

## Introduction

GitLab is used extensively to achieve regulatory compliance in the financial services industry. Many of the world's largest financial institutions are GitLab customers.
This page details the relevant rules, the principles needed to achieve them, and the features in GitLab that make that possible.

## Regulators and regulations

Examples of regulators include the following
1. America: [FEB](https://www.federalreserve.gov/) in the US, also see the [FFIEC IT Handbook](https://ithandbook.ffiec.gov/)
1. Europe: [FCA](https://www.fca.org.uk/) and [PRA](https://www.bankofengland.co.uk/prudential-regulation) in the UK, [FINMA](https://www.finma.ch/en/) in Switzerland
1. Asia: [MAS](http://www.mas.gov.sg/) in Singapore and [HKMA](http://www.hkma.gov.hk/eng/index.shtml)

Examples of relevant regulations include the following
1. GLBA Safeguards rule requires that financial institutions must protect the consumer information they collect and hold service providers to same standards.
1. Dodd-Frank’s purpose is to promote the financial stability of the United States by improving accountability and transparency in the financial system. It sets the baseline for what is “reasonable and appropriate” security around consumer financial data. You must be ready to prove your security controls and document them.
1. Sarbanes Oxley (SOX) exists to protect investors by improving the accuracy and reliability of corporate disclosures made pursuant to the securities laws, and for other purposes.  Advice for achieving this is augmented by other frameworks such as COBIT14 and the CIS Critical Security Controls.
1. PCI DSS is intended to maintain payment security and is required for all entities that store, process or transmit cardholder data. It requires companies using credit cards to protect cardholder data,  manage vulnerabilities, provide strong access controls, monitor and test, and maintain policy.

Specific controls common amongst these regulations are outlined below, along with features of GitLab that aid in their compliance.

## Separation of duties

### Rules

* To protect a system from unauthorized changes and fraud, more than one person is required to complete a task. 

### Principles

1. You never merge your own code.
1. All code needs to be peer reviewed.
1. Only authorized people can approve the code.
1. You need a log of who approved it. 

### GitLab features

1. [Protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html) 
1. [Merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html) 
1. [Unprotect permission](https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches) 
1. Future: [Approval jobs in CI pipelines](https://gitlab.com/gitlab-org/gitlab-ce/issues/44041) 
1. Future: [Two-person access controls](https://gitlab.com/gitlab-org/gitlab-ee/issues/7176)

## Security

### Rules

* Software needs to support evidence that data has not been modified.  
* Role-based access and revocation of accounts.  
* Auditing and logging of events in systems that process sensitive data.  
* Log system changes in a way that those logs are resistant to tampering and accessible only to privileged users.  

### Principles

1. Scan applications regularly for vulnerabilities.
1. Establish criteria for the prioritization of vulnerabilities and remediation activities.
1. Pay special attention to internally or custom developed applications with dynamic and static analysis.
1. Establish secure coding as a culture, and provide qualified training on secure coding.
1. Establish and document a secure development life-cycle approach that fits your business and developers.
1. Combine functional testing and security testing of applications: Assess for operational bugs and coding errors.

### GitLab features

1. [SAST](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html) 
1. [DAST](https://docs.gitlab.com/ee/user/project/merge_requests/dast.html) 
1. [Dependency scanning](https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html) 
1. [Container scanning](https://docs.gitlab.com/ee/user/project/merge_requests/container_scanning.html) 
1. [Security Dashboard](https://about.gitlab.com/2018/07/22/gitlab-11-1-released/#security-dashboard-for-projects) 

Also see our [security paradigm](https://about.gitlab.com/handbook/product/#security-paradigm) for more information on why GitLab security tools work better.

## Auditing 

### Rules 

* Systems must have clear audit logs to trace changes to the data and also to the logic flow.

### Principles 

1. Auditability of the production application: Software systems must generate all of the necessary logging information to construct a clear audit trail that shows how a user or entity attempts to access and utilize resources. 
1. Auditability of the software itself to detect changes in logic flow: Whether urban legend or not, the example is relevant of [the developer who pockets rounding errors to his own bank account](http://msgboard.snopes.com/cgi-bin/ultimatebb.cgi?ubb=get_topic;f=80;t=000015;p=0)  
1. Logs must be resistant to tampering and accessible only to privileged users. 

### GitLab features 

1. One concept of a user across the lifecycle to ensure the right level of permissions and access
1. [Audit logs](https://docs.gitlab.com/ee/administration/logs.html) 
1. [Audit events](https://docs.gitlab.com/ee/administration/audit_events.html) 
1. [Container image retention](https://docs.gitlab.com/omnibus/docker/README.html#where-is-the-data-stored) 
1. [Artifact retention](https://docs.gitlab.com/ee/administration/job_artifacts.html#storing-job-artifacts) 
1. [Test result retention](https://docs.gitlab.com/ee/development/testing_guide/end_to_end_tests.html#testing-code-in-merge-requests) 
1. Future: Disable squash of commits
1. Future: Prevent purge

## Licensed code usage

### Rules

* Third party and open source code use must comply with license constraints.

### Principles

1. To comply with license contraints, you must track license expiration and usage. This is important to manage risk from legal costs for license agreement violations and risk to your reputation.

### GitLab features
1. [License management](https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html#doc-nav) 

## Change management

### Rules

* All changes must be tracked. Sarbanes-Oxley specifically requires companies to notify the SEC of any material changes to the process that governs the flow of financial data. 

### Principles

1. Change Management is required with changes tracked, reviewed and approved.  
1. Changes should be made in such a way that they can be rolled back to a previous version quickly and easily. Here are two examples as to why: [Flash Crash](https://en.wikipedia.org/wiki/2010_Flash_Crash) and [TSB Bank Disaster](https://www.schneier.com/blog/archives/2018/04/tsb_bank_disast.html) 
Source control systems should prevent unauthorized changes using access control or, at least showing changes for a clear audit trail. 

### GitLab features

1. [Automated deploy](https://docs.gitlab.com/ee/topics/autodevops/index.html#auto-deploy) 
1. [Revert button](https://docs.gitlab.com/ee/update/6.3-to-6.4.html#1-revert-the-code-to-the-previous-version) 
1. [Review apps](https://docs.gitlab.com/ee/ci/review_apps/index.html#overview) make it easy to visualize the changes in code review and ensure changes function in a legitimate manner.

## State of art

### Rules

* While not exactly a rule per se, State of the art comes to play in Tort liability. 

### Principles

1. There are [some thoughts out there](https://en.wikipedia.org/wiki/State_of_the_art#Tort_liability), that by showing that you are using the state-of-the-art methods, you MAY help prove a point against negligence. 

### Features

1. GitLab is use in many large global financial services companies.
1. Both relevant US regulators run GitLab themselves.
1. 2,000 code contributors
1. 100,000 organizations
1. Millions of users

## Interested in GitLab?

[Contact sales](https://about.gitlab.com/sales)

# Reference articles of interest

1. [DevOps Survival](https://www.infoq.com/news/2016/07/devops-survival-finance)
1. [How the Federal Reserve Bank of New York Navigates the Supply Chain of Open Source](https://www.hpe.com/us/en/insights/articles/how-the-federal-reserve-bank-of-new-york-navigates-the-supply-chain-of-open-source-software-1710.html)
1. [Primer Ensuring Regulatory Compliance in Cloud Deployments](https://www.hpe.com/us/en/insights/articles/primer-ensuring-regulatory-compliance-in-cloud-deployments-1704.html)
1. [Understanding Security Regulations in the Financial Services Industry](https://www.sans.org/reading-room/whitepapers/analyst/understanding-security-regulations-financial-services-industry-37027)
1. [Regulatory Compliance Demystified](https://msdn.microsoft.com/en-us/library/aa480484.aspx#regcompliance_demystified_topic5)

