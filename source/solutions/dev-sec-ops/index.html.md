---
layout: markdown_page
title: "Integrating Security into DevSecOps"
---
## Application Security is hard - when it is a separate process

You want to test everything but you...
* Can’t afford what vendors charge to test that much code or that many users
* Lack security analysts to scan applications, validate results and triage for remediation
* Experience friction in process between app sec and engineering - dev won’t use a security tool and app sec only sees what the app sec tool shows.
* Can’t afford the time to scan large apps.

So you may...
* Only test externally-facing apps because they have higher risk
* Only test mission critical apps
* Only test major releases
* Focus on one testing method alone
* Hope incremental scanning will enable you to scan more code faster.

Even though you know that...
* Hackers can traverse laterally across your enterprise. They look for the weakest link most likely to be untested.
* Different types of scans are best at finding different types of vulnerabilities.
* You must strike a balance between risk and business agility.

You also want to shift left to empower developers to eliminate vulnerabilities at the source.  You may 
* Try embedding a security-checker in their IDE. But not everyone uses the same IDE. 
* Integrate your SAST scanner with your DevOps tools. Now your tool chain is even larger, more fragile and requires more resources to maintain. 

Balancing business velocity with security need not be a battle. 
- With GitLab security testing is built into the merge process.
- Every merge request is scanned for vulnerabilities in your code and that of your dependencies and licensing issues are identified.
- One source of truth for both the developer and the security pro.

When using GitLab CI/CD, every merge request is automatically scanned with the following:
* Static Application Security Testing [(SAST)](https://docs.gitlab.com/ee//user/project/merge_requests/sast.html) upon code commit
* Dynamic Application Security Testing [(DAST)](https://about.gitlab.com/product/dynamic-application-security-testing) via the review application in the CI/CD pipeline
* [Dependency Scanning](https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html) to find vulnerable Open Source components
* [Container Scanning](https://about.gitlab.com/product/container-scanning) of Docker images for security vulnerabilities
* [License Management](https://about.gitlab.com/product/license-management) to identify license usage that is out-of-policy

Resources:
* [Blog article: See how integration is the key to successful DevSecOps](https://about.gitlab.com/2018/09/11/what-south-africa-taught-me-about-cybersecurity/) 
* [Comparisons](about.gitlab.com/comparisons)
* [11.1 Release Radar with Security Level-set](https://about.gitlab.com/webcast/monthly-release/gitlab-11.1---security)
* [Security Deck](https://about.gitlab.com/handbook/marketing/product-marketing/#security-deck)

