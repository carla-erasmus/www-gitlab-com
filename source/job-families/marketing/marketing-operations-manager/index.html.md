---
layout: job_page
title: "Marketing Operations Manager"
---

You understand that a foundation of healthy growth in a fast-paced company is effective marketing operations. Your job is three-fold: evaluate/select/customize technology to enable effective marketing at GitLab, ensure high data quality and help colleagues access that data to enable smarter decisions, and assist in marketing analysis/planning/strategy.

## Responsibilities

* Marketing technology
  * Evaluate new marketing technology that can enable GitLab to grow its business faster and more efficiently.
  * Create documentation that guides the marketing team in their use of marketing software tools.
  * Train the marketing team on marketing software tools.
  * Audit use of marketing software tools with an eye towards continually improving how they are configured.
* Marketing data stewardship
  * Ensure we have documented processes in place that facilitate accurate data collection.
  * Review data quality across key dimensions that GitLab uses to evaluate its marketing performance.
  * Where data quality is lacking, identify the root cause and address systematically.
* Marketing analysis
  * Measure the marketing department's contribution to sales pipeline, and assess their performance throughout the entire funnel.
  * Measure the effectiveness of marketing campaigns and content, including ROI of marketing campaigns.
  * Measure the ratio of customer aquisition cost to customer lifetime value, by marketing tactic.
  * Assist with data-driven budgeting, planning, and strategy.

## Requirements

* Excellent spoken and written English.
* Experience with marketing automation software (Marketo highly preferred).
* Experience with CRM software (Salesforce preferred).
* Experience with modern marketing and sales development solutions such as LeanData, Bizible, and Outreach.
* Experience in sales and/or marketing teams of B2B software, Open Source software, and the developer tools space is preferred.
* Proficiency in MS Excel / Google Sheets.
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Be ready to learn how to use GitLab and Git
* You share our [values](/handbook/values), and work in accordance with those values.
