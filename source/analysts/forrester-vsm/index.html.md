---
layout: markdown_page
title: "Forrester VSM Wave 2018"
---
## GitLab and The Forrester New Wave™: Value Stream Management Tools, Q3 2018

This page represents how Forrester views our VSM product in relation to the larger market and how we're working with that information to build a better product.  It also provides Forrester with ongoing context for how our product is developing in relation to how they see the market.

### Forrester's Key Takeaways on the VSM Market at time of report publication:

**No Vendor Leads The Pack**
Forrester's research uncovered a market in which there are no Leaders; XebiaLabs, Plutora, CollabNet VersionOne, Tasktop, Targetprocess, and GitLab are Strong Performers; CloudBees, Intland Software, Jama Software, Blueprint Software Systems, and Panaya are Contenders; and Electric Cloud and CA Technologies are Challengers.

**Mapping, Visualization, And Product Vision Are The Biggest Differentiators In The Market**
VSM helps software development organizations visualize the development pipeline end to end. These tools capture, visualize, and analyze critical indicators of the speed and quality of software product creation. VSM is an emerging market: Vendors with a vision of empowering teams with analytics informed by real-world data and flexible planning schemes beat out those that focus on incremental enhancements within their traditional domains.

### Lessons Learned and Future Improvements

Forrester helped us understand a few areas where we need to improve. See our thoughts
and upcoming work in each.

#### Mapping

The VSM model encourages organizations to map business value, people, processes,
and data across value streams. In particular, this aids in efficient resource allocation,
and visibility of areas requiring urgent improvement.

GitLab is well-positioned to help organizations in this area since many of the required
building blocks are already built into GitLab. These include issues, boards, merge 
requests, Git artifacts (commits and branches), and pipelines. GitLab needs to improve 
integrating these pieces together for the purpose of mapping value streams, by building
a layer of seamless functionality, in order to provide that mapping visibility.

See [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=opened&label_name[]=mapping) 
and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=mapping) 
in the area of mapping.

#### Analytics and Visualization

The value streams in VSM need to be based on consistent and reliable data. The VSM
solution needs to capture this data as the processes are excecuted, synthesize it
into actionable insight, and present the results in useful visualizations appropriate
for the various team members and stakeholders in an organization. For example, these 
can be detailed metrics relevant to a small development team such as a burndown
chart, or a director-level or executive-level rollup such as an executive-level
dashbord showing mean total cycle delivery times to customer in the past 8 quarters.

Additionally, the VSM solution needs to be predictive based on historical data. 
It should account for what-if scenarios and risk assessments, considering alternative 
business decisions or process iterations.

We are focused on bringing this functionality to GitLab. In particular, we will
build the features in their relevant and optimal locations in GitLab. For example, 
burndown charts will be integrated into issue boards, where development teams are 
already working. Director-level dashboards, on the other hand will be highly customizable
and located at the group level, where the scope can span many children/descendant
groups and projects reflecting the organization structure as needed.

See [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=opened&label_name[]=analytics) 
and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=analytics) 
in the areas of analytics and visualization.

#### Governance

Big enterprises rely on a governance framework to mitigate a variety of risks. If 
an organization breaks a controlled process while shipping a new piece of software, 
which in turn results in a catastrophic event, causing a significant business interruption 
or loss of business potential, that value delivered from the shipped software is 
rendered nil and actually negative.

GitLab therefore needs to support these enterprise governance standards through 
features such as enforced workflows, approvals, templating of various artifacts 
like issues and code files, compliance, traceability of requirements, and 
auditing/logging.

See [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=opened&label_name[]=governance) 
and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=governance) 
in the area of governance.

