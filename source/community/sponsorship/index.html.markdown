---
layout: markdown_page
title: "GitLab Diversity Sponsorship"
comments: false
sharing: true
suppress_header: true
---

### Diversity Sponsorship Criteria 
GitLab is striving to promote increased awareness of diversity issues, in effort to meet this goal, GitLab will sponsor informative, interactive and reflective events.

### Requirements 
* The Program must reflect respect of differences in Race, Ethnic Background, Sexual Orientation, Gender and ability/disability.
* Events must be informative. The take-away must be a learning experience as it relates to diversity. 
* Attendees must be able to answer: "What did I learn about diversity?"
* The Event must be interactive and encourage audience participation. This may include lectures, discussions, demonstrations, and or/Q&A sessions.
* Attendees must have the opportunity to participate in discussions, ask questions, get answers, interact through demonstrations, learning exercises, roundtables, etc.
* The Event must contain a reflective element that introduces participants to a new perspective or idea. The reflective sessions can be roundtables sessions and or Q&A sessions.
* Attendees must have the ability to revisit the process and information learned and understand practical applications to diversity in their everyday life.

The Event must help to further GitLab's values. In the application please include specific connections/references to GitLab's values. 

![Community Sponsorship](/images/community/gitlab-growth.jpg)

If you intend to apply, please leave enough lead time (at least 4 weeks but
preferably more) for us to process your application.

[Apply for sponsorship](https://docs.google.com/forms/d/1FUm7DOc85yjplFj4zAIo3pqlGlbJR4c6AnHDHVv0k7Y/viewform)

Why is fostering diversity important?

As if the moral imperative and ethical rationale was not enough,
there are also practical advantages to fostering diversity.
These events help increase the potential pool of talent to work at GitLab.
Research has also proven that more diversity is better for business in almost
every aspect. ([McKinsey, 2015](http://www.mckinsey.com/insights/organization/why_diversity_matters))

### Eligibility (Non-Diversity Events)

If you're hosting an event which doesn't meet the description of an "event which promotes diversity in technology", we'd like to thank you for considering GitLab as your sponsor, unfortunately we don't respond to non-diversity requests for sponsoring. We identify sponsorship opportunities ourselves and reach out ourselves.

### Sponsored Events

You can take a look at the diversity events we sponsor on our [events page](/events/).

